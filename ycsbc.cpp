//
//  ycsbc.cc
//  YCSB-C
//
//  Created by Jinglei Ren on 12/19/14.
//  Copyright (c) 2014 Jinglei Ren <jinglei@ren.systems>.
//  Ju: Tailored the code for LevelDB <chenju2k6@gmail.com>

#include <cstring>
#include <string>
#include <iostream>
#include <vector>
#include <future>
#include <atomic>
#include <sstream>
#include <unordered_map>
#include "utils.h"
#include "properties.h"
#include "timer.h"
#include "client.h"
#include "ycsb_db.h"
#include "core_workload.h"
#include "db_factory.h"
extern std::atomic<long> totalL;
using namespace std;

const unsigned int BLOCK_POLLING_INTERVAL = 2;
const unsigned int CONFIRM_BLOCK_LENGTH = 5;
const unsigned int HL_CONFIRM_BLOCK_LENGTH = 1;
const unsigned int PARITY_CONFIRM_BLOCK_LENGTH = 1;

std::unordered_map<string, double> pendingtx;
// locking the pendingtx queue
SpinLock txlock;

void UsageMessage(const char *command);
bool StrStartWith(const char *str, const char *pre);
string ParseCommandLine(int argc, const char *argv[], utils::Properties &props);

utils::Timer<double> stat_timer;

int DelegateClient(ycsbc::DB *db, ycsbc::CoreWorkload *wl, const int num_ops,
    bool is_loading, const int txrate) {
  db->Init();
  ycsbc::Client client(*db, *wl);
  int oks = 0;
  for (int i = 0; i < num_ops; ++i) {
    if (is_loading) {
      oks += client.DoInsert();
    } else {
      oks += client.DoTransaction();
    }
  }
  db->Close();
  return oks;
}


int main(const int argc, const char *argv[]) {
  utils::Properties props;
  string file_name = ParseCommandLine(argc, argv, props);

  ycsbc::DB *db = ycsbc::DBFactory::CreateDB(props);
  if (!db) {
    cout << "Unknown database name " << props["dbname"] << endl;
    exit(0);
  }

  db->Init(&pendingtx, &txlock);

  ycsbc::CoreWorkload wl;
  wl.Init(props);

  const int num_threads = stoi(props.GetProperty("threadcount", "1"));
  const int txrate = stoi(props.GetProperty("txrate", "10"));

  utils::Timer<double> stat_timer;

  if (props["dbname"] == "leveldb") {
    // Loads data
    vector<future<int>> actual_ops;
    int total_ops = stoi(props[ycsbc::CoreWorkload::RECORD_COUNT_PROPERTY]);
    int tx_ops = stoi(props[ycsbc::CoreWorkload::OPERATION_COUNT_PROPERTY]);
    cout << "total_ops=" << total_ops << " and tx_ops" << tx_ops << endl;
    int sum = 0;
    if (props["load"] == "1") {
      for (int i = 0; i < num_threads; ++i) {
	actual_ops.emplace_back(async(launch::async, DelegateClient, db, &wl,
	      total_ops / num_threads, true, txrate));
      }

      for (auto &n : actual_ops) {
	assert(n.valid());
	sum += n.get();
      }
      cout << "# Loading records :\t" << sum << endl;
    } else if (props["load"] == "0" ){
      // transaction
      actual_ops.clear();
      long start_time = utils::time_now();
      for (int i = 0; i < num_threads; ++i) {
	actual_ops.emplace_back(async(launch::async, DelegateClient, db, &wl,
	      tx_ops / num_threads, false, txrate));
      }

      sum = 0;
      for (auto &n : actual_ops) {
	assert(n.valid());
	sum += n.get();
      }
      long end_time = utils::time_now();
      cout << "# Transaction records :\t" << sum << endl;
      cout << "# Transaction throughput =\t" << total_ops/((end_time-start_time)/1000000000.0) << " ops/s" << endl;
      printf("tx_ops is %d and totalL is %ld\n",tx_ops,totalL.load());
      cout << "# Transaction latency =\t" << totalL.load()/tx_ops << " ns/op" << endl;
    } else {
      //db->Compact();
      cout << "Compaction Complete!\n" << endl;
    }
  } 
}

string ParseCommandLine(int argc, const char *argv[],
    utils::Properties &props) {
  int argindex = 1;
  string filename;
  while (argindex < argc && StrStartWith(argv[argindex], "-")) {
    if (strcmp(argv[argindex], "-threads") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("threadcount", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-db") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("dbname", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-load") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("load", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-endpoint") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("endpoint", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-txrate") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("txrate", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-wl") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("workload", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-wt") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      props.SetProperty("deploy_wait", argv[argindex]);
      argindex++;
    } else if (strcmp(argv[argindex], "-P") == 0) {
      argindex++;
      if (argindex >= argc) {
	UsageMessage(argv[0]);
	exit(0);
      }
      filename.assign(argv[argindex]);
      ifstream input(argv[argindex]);
      try {
	props.Load(input);
      } catch (const string &message) {
	cout << message << endl;
	exit(0);
      }
      input.close();
      argindex++;
    } else {
      cout << "Unknown option '" << argv[argindex] << "'" << endl;
      exit(0);
    }
  }

  if (argindex == 1 || argindex != argc) {
    UsageMessage(argv[0]);
    exit(0);
  }

  return filename;
}

void UsageMessage(const char *command) {
  cout << "Usage: " << command << " [options]" << endl;
  cout << "Options:" << endl;
  cout << "  -threads n: execute using n threads (default: 1)" << endl;
  cout << "  -db dbname: specify the name of the DB to use (e.g., leveldb)"
    << endl;
  cout << "  -P propertyfile: load properties from the given file. Multiple "
    "files can be specified" << endl;
  cout << "  -load 1 means load, 0 means transaction" << endl;
}


inline bool StrStartWith(const char *str, const char *pre) {
  return strncmp(str, pre, strlen(pre)) == 0;
}
