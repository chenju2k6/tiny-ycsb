#include "hist.h"
#include "ycsb_db.h"
#include "utils.h"
#include <thread>
#include <chrono>
//#include "leveldb/db.h"

using namespace std;
std::atomic<long> totalL(0);
//TODO define a db instance
//leveldb::DB *global_;

namespace ycsbc {

  hist::hist() {
    //TODO open
    std::cout<<"Open db" << std::endl;
    //leveldb::Options options;
    //options.create_if_missing = true;
    //leveldb::DB::Open(options,"/home/ju/sysbase",&global_);
  }

  /// ignore table
  /// ignore field
  /// read value indicated by a key
  int hist::Read(const string &table, const string &key,
      const vector<string> *fields, vector<KVPair> &result) {
    std::string val;
    long st = utils::time_now();
   //global_->Get(leveldb::ReadOptions(),key,&val);
    long et = utils::time_now();
    long cur = totalL.load();
    cur += (et-st);
    totalL.store(cur);
    return DB::kOK;
  }

  // ignore table
  // update value indicated by a key
  int hist::Update(const string &table, const string &key,
      vector<KVPair> &values) {
    long st = utils::time_now();
    string val = "";
    int count = 0;
    static int count1=0;
    for (auto v : values) {
      count++;
      val += v.first + "=" + v.second + " ";
    }
    //put TODO 
    //global_->Put(leveldb::WriteOptions(),key,val);
    long et = utils::time_now();
    long cur = totalL.load();
    cur += (et-st);
    totalL.store(cur);
    return DB::kOK;
  }
	

  // ignore table
  // ignore field
  // concate values in KVPairs into one long value
  int hist::Insert(const string &table, const string &key,
      vector<KVPair> &values) {
    return Update(table, key, values);
  }

  // ignore table
  // delete value indicated by a key
  int hist::Delete(const string &table, const string &key) {
    vector<KVPair> empty_val;
    return Update(table, key, empty_val);
  }

  // get all tx from the start_block until latest
  vector<string> hist::PollTxn(int block_number) {
    vector<string> v;
    return v;
  }


  unsigned int hist::GetTip() { return 0;}
}  // ycsbc
